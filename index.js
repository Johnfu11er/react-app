require('dotenv').config()
const express = require("express");
const app = express();
var static = process.env.data
var app_version = process.env.version
var app_tag = process.env.tag

app.get("/", (req, res) => {
    res.send(`
    <body style="text-align:center">
    <div style="padding:5px"></div>
    <h1>Static variable: ${static}</h1>
    <h1>App Version: ${app_version}</h1>
    </body>`);
})

app.listen("3000", () => {
    console.log("Server is running on port 3000");
})